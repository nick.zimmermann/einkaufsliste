export const lightTheme = {
  body: '#fff',
  text: '#363537',
  toggleBorder: '#bbb',
  gradient: 'linear-gradient(#39598A, #79D7ED)',
  suggestions: '#fff',
  suggestionsHigh: '#ddd',
  listItem: '#fff',
  listItemBorder: 'rgba(0,0,0,.125)',
};

export const darkTheme = {
  body: '#363537',
  text: '#FAFAFA',
  toggleBorder: '#6B8096',
  gradient: 'linear-gradient(#091236, #1E215D)',
  suggestions: '#474648',
  suggestionsHigh: '#555',
  listItem: '#474648',
  listItemBorder: 'rgba(255,255,255,.25)',
};
