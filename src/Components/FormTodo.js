import React from 'react';
import { Form, Row } from 'react-bootstrap';
import Suggestions from './Suggestions';

function FormTodo(props) {
  // Check Input, setError and add new Todo
  const handleSubmit = (e) => {
    e.preventDefault();
    // Check if input is not empty
    if (props.input) {
      props.setError({ ...props.error, show: false });
      props.addTodo(props.input);
    } else {
      props.setError({ show: true, text: 'Enter item...' });
    }
    props.setInput('');
  };

  return (
    <>
      <div className="mt-3">
        <span className="position-absolute d-flex align-items-center m-0">
          {props.themeIcon}
        </span>
        <h2 className="text-center mb-3">To-Do List</h2>
      </div>
      <Row className="align-items-center justify-content-between px-3">
        <Form onSubmit={handleSubmit} className="formTodoForm">
          <Suggestions
            input={props.input}
            setInput={props.setInput}
            suggestions={props.todosSuggestions}
            placeholder="Add item"
          />
        </Form>
        <div className="d-flex justify-content-end">{props.children}</div>
      </Row>
      {props.error.show && <small className="text-danger">{props.error.text}</small>}
      <div className="mt-3 text-right lead">Total Items: {props.totalItems}</div>
    </>
  );
}

export default FormTodo;
