import React from 'react';
import {
  CheckSquare,
  Trash,
  XSquare,
  InfoCircle,
  PlusCircle,
  DashCircle,
  CircleFill,
} from 'react-bootstrap-icons';
import ReactTooltip from 'react-tooltip';

function TodoItem({ todo, size, todos, setTodos, changeAmount, theme }) {
  // Functions
  const completeHandler = () => {
    // Find the todo that was clicked and toggle the done property
    setTodos(
      todos.map((item) => {
        if (item.id === todo.id) {
          return {
            ...item,
            done: !item.done,
          };
        }
        return item;
      })
    );
  };

  const priorityHandler = () => {
    // Find the todo that was clicked and toggle the done property
    setTodos(
      todos.map((item) => {
        if (item.id === todo.id) {
          return {
            ...item,
            priority: !item.priority,
          };
        }
        return item;
      })
    );
  };

  const deleteHandler = () => {
    // Remove clicked item with filter
    setTodos(todos.filter((el) => el.id !== todo.id));
  };

  return (
    <li className="list-group-item listItem">
      <div className="d-flex justify-content-between align-items-center">
        <div className="d-flex align-items-center textSize">
          <CircleFill
            color={todo.priority ? 'red' : 'green'}
            onClick={priorityHandler}
            className="mr-2"
            size={15}
            style={{ flexShrink: 0 }}
          />
          <span className={todo.done ? 'completed todoItemName' : 'todoItemName'}>
            <span data-tip={todo.name}>{todo.name}</span>
            <ReactTooltip type={theme === 'light' ? 'dark' : 'light'} effect="solid" />
          </span>
        </div>
        <div>
          <span className={todo.done ? 'mr-3 completed' : 'mr-3'}>
            <DashCircle
              size={30}
              onClick={() => changeAmount(todo.done, false, todo.id)}
            />
            <span className="ml-2 mr-2" style={{ fontSize: '15px' }}>
              {todo.amount}
            </span>
            <PlusCircle
              size={30}
              onClick={() => changeAmount(todo.done, true, todo.id)}
            />
          </span>
          <InfoCircle
            size={30}
            className="mr-3 d-none d-sm-inline"
            data-tip={'Created at: ' + todo.date}
          />
          <ReactTooltip
            place="top"
            type={theme === 'light' ? 'dark' : 'light'}
            effect="solid"
          />
          {todo.done ? (
            <XSquare size={size} onClick={completeHandler} color="red" className="mr-3" />
          ) : (
            <CheckSquare
              size={size}
              onClick={completeHandler}
              color="green"
              className="mr-3"
            />
          )}

          <Trash size={size} onClick={deleteHandler} color="red" />
        </div>
      </div>
    </li>
  );
}

export default TodoItem;
