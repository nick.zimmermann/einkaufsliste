import React from 'react';
import { Dropdown } from 'react-bootstrap';

function DropdownMenu({ filter, setFilter }) {
  return (
    <Dropdown>
      <Dropdown.Toggle variant="success" id="dropdown-basic" style={{ width: '135px' }}>
        {filter}
      </Dropdown.Toggle>

      <Dropdown.Menu className="dropDown">
        <Dropdown.Item className="dropDown" onClick={() => setFilter('All')}>
          All
        </Dropdown.Item>
        <Dropdown.Item className="dropDown" onClick={() => setFilter('Completed')}>
          Completed
        </Dropdown.Item>
        <Dropdown.Item className="dropDown" onClick={() => setFilter('Uncompleted')}>
          Uncompleted
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default DropdownMenu;
