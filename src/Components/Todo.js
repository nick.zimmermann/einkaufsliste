import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import useLocalStorage from './useLocalStorage';
// Components Import
import TodoList from './TodoList';
import FormTodo from './FormTodo';
import DropdownMenu from './DropdownMenu';

function Todo(props) {
  const TodosEx = [
    'Brot',
    'Milch',
    'Fleisch',
    'Rind',
    'Käse',
    'Yogurt',
    'Schokolade',
    'Chips',
    'Spaghetti',
    'Tomatensauce',
    'Reis',
    'Wasser',
    'Mehl',
    'Kaffee',
    'Aufschnitt',
    'Rindfleischetikettierungsüberwachungsaufgabenübertragungsgesetz',
  ];

  const [input, setInput] = useState('');
  const [error, setError] = useState({ show: false, text: '' });
  const [filter, setFilter] = useState('All');
  const [todos, setTodos] = useLocalStorage('todos', []);
  const [filteredTodos, setFilteredTodos] = useState([]);
  const [todosSuggestions, setTodosSuggestions] = useLocalStorage('suggestions', TodosEx);
  const [totalItems, setTotalItems] = useState(0);

  // Run UseEffect when Filter or Todos change
  useEffect(() => {
    handleFilter();
    /* eslint-disable-next-line */
  }, [todos, filter]);

  useEffect(() => {
    setTotalItems(countTotalItem());
    /* eslint-disable-next-line */
  }, [filteredTodos]);

  const countTotalItem = () => {
    return filteredTodos.reduce((total, item) => total + item.amount, 0);
  };

  // Change FilteredTodos with Filter
  const handleFilter = () => {
    switch (filter) {
      case 'Completed':
        setFilteredTodos(todos.filter((todo) => todo.done));
        break;
      case 'Uncompleted':
        setFilteredTodos(todos.filter((todo) => !todo.done));
        break;
      default:
        setFilteredTodos(todos);
        break;
    }
  };

  // Add Todo to Todoslist
  const addTodo = (name) => {
    // Check if todo is already in the Todo list
    if (
      !todos.find((todo) => todo.name.trim().toLowerCase() === name.trim().toLowerCase())
    ) {
      const newTodo = {
        id: todos.length !== 0 ? Math.max(...todos.map((l) => l.id)) + 1 : 0,
        name,
        done: false,
        date: new Date().toLocaleDateString(),
        amount: 1,
        priority: false,
      };
      setTodos([...todos, newTodo]);
    } else {
      setError({ show: true, text: 'Item already added' });
    }

    // Check if Todo is already in Suggestions list
    if (
      !todosSuggestions.find(
        (el) => el.trim().toLowerCase() === name.trim().toLowerCase()
      )
    ) {
      setTodosSuggestions([...todosSuggestions, name]);
    }
  };

  const changeAmount = (completed, increase, id) => {
    if (completed) return;
    setTodos(
      todos.map((item) => {
        if (item.id === id) {
          if (item.amount === 1 && increase === false) {
            return item;
          } else {
            return {
              ...item,
              amount: increase ? item.amount + 1 : item.amount - 1,
            };
          }
        }
        return item;
      })
    );
  };

  return (
    <Container>
      <FormTodo
        setInput={setInput}
        input={input}
        addTodo={addTodo}
        error={error}
        setError={setError}
        todosSuggestions={todosSuggestions}
        totalItems={totalItems}
        themeIcon={props.children}
      >
        {/* Dropdown as Childern */}
        <DropdownMenu filter={filter} setFilter={setFilter} />
      </FormTodo>

      <TodoList
        size={30}
        todos={todos}
        setTodos={setTodos}
        filteredTodos={filteredTodos}
        changeAmount={changeAmount}
        theme={props.theme}
      />
    </Container>
  );
}

export default Todo;
