import React from 'react';
import TodoItem from './TodoItem';

function TodoList({ filteredTodos, todos, setTodos, size, changeAmount, theme }) {
  return (
    <>
      <ul className="list-group mt-3 mb-5">
        {filteredTodos.length > 0 ? (
          filteredTodos
            .sort((a, b) => a.id - b.id)
            .sort((a, b) => (a.priority === b.priority ? 0 : a.priority ? -1 : 1))
            .map((todo) => (
              <TodoItem
                key={todo.id}
                size={size}
                todos={todos}
                setTodos={setTodos}
                todo={todo}
                changeAmount={changeAmount}
                theme={theme}
              />
            ))
        ) : (
          <div className="text-center">Keine Items vorhanden...</div>
        )}
      </ul>
    </>
  );
}

export default TodoList;
