import React from 'react';

import { BrightnessHigh, Moon } from 'react-bootstrap-icons';

const Toggle = ({ theme, toggleTheme }) => {
  const isLight = theme === 'light';
  return (
    <div className="mt-2">
      {isLight ? (
        <Moon onClick={toggleTheme} size={28} style={{ cursor: 'pointer' }} />
      ) : (
        <BrightnessHigh onClick={toggleTheme} size={28} style={{ cursor: 'pointer' }} />
      )}
    </div>
  );
};

export default Toggle;
