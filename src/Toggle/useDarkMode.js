import useLocalStorage from '../Components/useLocalStorage';

function useDarkMode() {
  const [theme, setLocalTheme] = useLocalStorage('theme', 'light');

  const setTheme = () => {
    setLocalTheme(theme === 'light' ? 'dark' : 'light');
  };

  return [theme, setTheme];
}

export default useDarkMode;
