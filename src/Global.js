import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  *,
  *::after,
  *::before {
    box-sizing: border-box;
  }

  body {
    background: ${({ theme }) => theme.body};
    color: ${({ theme }) => theme.text};
    height: 100vh;
    margin: 0;
    padding: 0;
    font-family: BlinkMacSystemFont, -apple-system, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;
    transition: all 0.25s linear;
  }
  
  .react-autosuggest__input--focused {
  outline: none;
}

.react-autosuggest__input {
  background: ${({ theme }) => theme.suggestions};
  color: ${({ theme }) => theme.text};
}

.react-autosuggest__input--open {
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
}

.react-autosuggest__suggestions-container {
  display: none;
}

.react-autosuggest__suggestions-container--open {
  display: block;
  position: absolute;
  /* top: 51px; */
  width: 240px;
  border-top: none;
  background: ${({ theme }) => theme.suggestions};
  border: 1px solid ${({ theme }) => theme.toggleBorder};
  font-family: Helvetica, sans-serif;
  font-weight: 300;
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
  z-index: 2;
}

.react-autosuggest__suggestions-list {
  margin: 0;
  padding: 0;
  list-style-type: none;
}

.react-autosuggest__suggestion {
  cursor: pointer;
  padding: 10px 20px;
}

.react-autosuggest__suggestion--highlighted {
  background-color: ${({ theme }) => theme.suggestionsHigh}
}

@media (max-width: 430px) {
  .react-autosuggest__input {
    width: 100%;
  }
  .react-autosuggest__suggestions-container--open {
    width: 100%;
  }
  .formTodoForm {
    width: 50%;
  }
}

.listItem {
  background: ${({ theme }) => theme.listItem};
  color: ${({ theme }) => theme.text};
  border: 1px solid ${({ theme }) => theme.listItemBorder};
}

.dropDown {
  background: ${({ theme }) => theme.body};
  color: ${({ theme }) => theme.text};
}
  
  `;
