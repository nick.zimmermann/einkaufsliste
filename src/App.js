import { ThemeProvider } from 'styled-components';
import './App.css';
import Todo from './Components/Todo';
import { GlobalStyles } from './Global';
import { lightTheme, darkTheme } from './Theme';
import Toggle from './Toggle/Toggle';
import useDarkMode from './Toggle/useDarkMode';

function App() {
  const [theme, setTheme] = useDarkMode();
  const themeMode = theme === 'light' ? lightTheme : darkTheme;
  return (
    <>
      <ThemeProvider theme={themeMode}>
        <GlobalStyles />
        <Todo theme={theme}>
          <Toggle theme={theme} toggleTheme={setTheme} />
        </Todo>
      </ThemeProvider>
    </>
  );
}

export default App;
